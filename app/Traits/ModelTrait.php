<?php


namespace App\traits;


trait ModelTrait
{
    public function createData($data)
    {
        return $this::create($data);
    }

    public function insertData($data)
    {
        return $this->insert($data);
    }

    public function updateData($id, $data)
    {
        return self::updateOrCreate(['id' => $id], $data);
    }

}
