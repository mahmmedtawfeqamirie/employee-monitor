<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Dashboard
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->is_Admin && auth()->user()->status)
            return $next($request);
        else if(Auth::check()&&Auth::user()->is_Admin ) {
            Auth::logout();
            return redirect('');
        }

        return back();
    }
}
