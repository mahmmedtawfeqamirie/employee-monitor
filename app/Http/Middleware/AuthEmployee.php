<?php

namespace App\Http\Middleware;

use App\Models\Users\Schedule;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $schedule=new Schedule();
        if (auth()->check() && auth()->user()->status && ($schedule->isLoggedInToday(auth()->user()->id)||auth()->user()->is_Admin))
            return $next($request);
        else if(auth()->check() && ($schedule->isLoggedInToday(auth()->user()->id)||auth()->user()->is_Admin)) {
            Auth::logout();
            return redirect('');
        }
        return redirect('profile/' . Auth::id());
    }
}
