<?php

namespace App\Http\Controllers\Users;

use App\Http\Requests\CreateMissionRequest;
use App\Http\Requests\FilterDateMonthlyRequest;
use App\Http\Requests\NotAttendReasonRequest;
use App\Http\Requests\UpdateLogoutRequest;
use App\Models\Activity\Mission;
use App\Models\Activity\NonAttendence;
use App\Models\Activity\Rating;
use App\Models\User;
use App\Models\Users\Schedule;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\Param;

class ProfileController extends Controller
{
    protected $users, $attends, $schedule, $missions, $ratings;

    /**
     * ProfileController constructor.
     * @param User $users
     * @param NonAttendence $attende
     * @param Schedule $schedule
     * @param Mission $mission
     * @param Rating $rating
     */
    public function __construct(User $users, NonAttendence $attende, Schedule $schedule, Mission $mission, Rating $rating)
    {
        $this->users = $users;
        $this->attends = $attende;
        $this->schedule = $schedule;
        $this->missions = $mission;
        $this->ratings = $rating;
    }

    /**
     * @param $id
     * user -> specific id request
     * worktime -> get hours this month duration [login-logout]
     * rate -> average records rates with specific user_id
     * missionsCount -> num of missions with status=done
     * @return Application|Factory|View|Param Profile Page
     */
    public function showProfile($id)
    {

        $user = $this->users->getObjectUser(['id' => $id]);

        $this->authorize('update', $user);

        $count = count($this->attends->getAbsent($user->id));

        if (!(Auth::user()->is_Admin)) {

            if ($count)
                return view('Employee.nonAttendce', compact(['count', 'user']));

            //Check if user is logout yesterday
            $lastLogin = $this->schedule->getLastLogin($user->id);

            if (!empty($lastLogin->login) && empty($lastLogin->logout)) {
                $logintime = date('H:i', strtotime("$lastLogin->login"));
                return view('Employee.forgetLogout', compact(['count', 'user', 'lastLogin', 'logintime']));
            }

            if ($this->schedule->checkLogoutStatus($id) || empty($this->schedule->isLoggedInToday($id)))
                $this->schedule->insertData(['login' => Carbon::now()->toDateTime(), 'user_id' => $id]);
        }
        $workTime = $this->schedule->getWorkTime($id);
        $rate = $this->ratings->getRateUser($id);
        $missionsCount = $this->missions->getDataDoneThisMonthByUserId($id);

        return view('Employee.profile', compact(['user', 'workTime', 'rate', 'missionsCount']));
    }

    /**
     * @param $id -> userId
     * send all missions with filter today In addition attendences only for admin
     * @return Application|Factory|View Monthly Missions
     */
    public function monthlyMissions($id)
    {
        $user_id = $id;

        $user = $this->users->getObjectUser(['id' => $user_id]);
        $this->authorize('update', $user);

        $headDataTable[1] = collect([['name' => 'Description'], ['name' => 'Status'], ['name' => 'Time']]);
        if (!Auth::user()->is_Admin)
            $headDataTable[1]->push(['name' => 'Action']);

        $headDataTable[2] = collect([['name' => 'Description'], ['name' => 'Time']]);

        $data[1] = $this->missions->getData($user_id, now());
        $data[2] = $this->attends->getDataByUserId($user_id, now());

        return view('Employee.monthlyMission', compact(['user_id', 'headDataTable', 'data']));
    }

    /**
     * @param FilterDateMonthlyRequest $request
     * request -> user_id
     * request -> date you want to filter data with
     * @return JsonResponse
     */
    public function updateMonthlyMissions(FilterDateMonthlyRequest $request)
    {
        $user_id = $request->get('user_id');
        $date = $request->get('date');

        $filterDate = Carbon::createFromFormat('m/Y', $date);

        $data[1] = $this->missions->getData($user_id, $filterDate);
        $admin = Auth::user()->is_Admin;

        $data[2] = $this->attends->getDataByUserId($user_id, $filterDate);

        return response()->json(compact(['data', 'admin']));
    }

    /**
     * @param CreateMissionRequest $request
     * userId & description & status of mission
     * id = 0 means create mission or edit status of mission
     * @return JsonResponse
     */
    public
    function createMission(CreateMissionRequest $request)
    {
        $data = $request->only('user_id', 'description', 'status');
        $id = $request->get('id', 0);

        $mission = $this->missions->updateData($id, $data);
        $admin = Auth::user()->is_Admin;

        return response()->json([compact('mission', 'admin')]);
    }

    /**
     * @param NotAttendReasonRequest $request
     * request -> message reason
     * @return Application|RedirectResponse|Redirector Profile Page
     */
    public
    function saveNotAttendReason(NotAttendReasonRequest $request)
    {
        $userId = Auth::id();
        $this->attends->updateAbsent($userId, ['description' => $request->get('Reason')]);
        return redirect('profile/' . Auth::id());
    }

    /**
     * @param UpdateLogoutRequest $request
     * request -> old Login
     * request -> new Logout
     * @return Application|RedirectResponse|Redirector
     */
    public
    function saveUpdateLogout(UpdateLogoutRequest $request)
    {
        $date = date('Y-m-d', strtotime($request->get('forgLogin')));
        $time = date('H:i:s', strtotime($request->get('forgLogout')));
        $data['logout'] = date('Y-m-d H:i:s', strtotime("$date $time"));

        $this->schedule->updateForgetLogout($request->get('forgLogin'), ['logout' => $data['logout']]);

        return redirect('profile/' . Auth::id());
    }
}
