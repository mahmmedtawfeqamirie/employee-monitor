<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Models\Users\Schedule;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $users, $schedules;

    /**
     * AuthController constructor.
     * @param User $users
     * @param Schedule $schedule
     */
    public function __construct(User $users, Schedule $schedule)
    {
        $this->users = $users;
        $this->schedules = $schedule;
    }

    /**
     *return Welcome Page Or Dashboard or profile
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function welcome()
    {
        if (!auth()->check())
            return view('welcome.welcome');

        if (auth()->user()->is_Admin)
            return redirect('dashboard');
        return redirect('profile/' . Auth::id());
    }

    /**
     * return LoginPage or Dashboard or profile
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function viewLogin()
    {
        if (!auth()->check())
            return view('AuthUser.loginPage');

        if (auth()->user()->is_Admin)
            return redirect('dashboard');
        return redirect('profile/' . Auth::id());
    }

    /**
     * @param LoginRequest $request
     * user id & password
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $UserData = $request->only('id', 'password');
        $userId = $UserData['id'];
        $this->users->updateData($userId, ['status' => 1]);

        if ($this->users->authentication($UserData)) {
            $admin = false;
            if (auth()->user()->is_Admin)
                $admin = true;
            return response()->json(compact(['admin', 'userId']));
        }
        return response()->json('error', 404);
    }

    /**
     * return Logout Page
     * @return Application|RedirectResponse|Redirector
     */
    public function logout()
    {
        if(Auth::check()) {

            $userId = auth()->user()->id;
            if (!(auth()->user()->is_Admin))
                $this->schedules->saveUserLogoutTime($userId);

            $this->users->updateData($userId, ['status' => 0]);
            auth()->logout();
        }
        return view('AuthUser.logoutPage');
    }
}
