<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilterDateRequest;
use App\Http\Requests\RateScoreRequest;
use App\Models\Activity\Mission;
use App\Models\Activity\Rating;
use App\Models\User;
use App\Models\Users\Schedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;


class ActivityController extends Controller
{
    protected $schedules, $missions, $rates, $users;

    /**
     * ActivityController constructor.
     * @param Schedule $schedule
     * @param Mission $mission
     * @param Rating $rate
     * @param User $user
     */
    public function __construct(Schedule $schedule, Mission $mission, Rating $rate, User $user)
    {
        $this->schedules = $schedule;
        $this->missions = $mission;
        $this->rates = $rate;
        $this->users = $user;
    }

    /**
     * @param $userId
     * return Activity Page with table schedules & missions
     * @return Application|Factory|View
     */
    public function showActivity($userId)
    {
        $user = $this->users->getObjectUser(['id' => $userId]);
        $this->authorize('update', $user);

        $headTableSection = collect
        ([
            ['name' => ''],
            ['name' => 'First Login'],
            ['name' => 'Last Logout'],
            ['name' => 'Duration']
        ]);

        $headNormalTable = collect([['name' => 'Mission'], ['name' => 'Status']]);

        $first = "Not yet";
        $last = "Until Now";

        $schedule = $this->schedules->getSchedule($userId, now());
        $duration = $this->schedules->getDuration($userId, now());
        $missions = $this->missions->getMissions($userId, now());

        if ($schedule) {
            $first = $this->schedules->getFirstLogin($schedule);
            $last = $this->schedules->getLastLogout($schedule);
        }
        return view('Employee.activity',
            compact(['headTableSection', 'headNormalTable', 'missions', 'schedule', 'first', 'last', 'duration', 'userId']));

    }

    /**
     * @param FilterDateRequest $request
     * request -> date with filter date on tables missions & schedules
     * request -> id -> user id
     * @return JsonResponse
     */
    public function missUpdate(FilterDateRequest $request)
    {
        try {
            $date = $request->get('date');
            $date = Carbon::createFromFormat('d/m/Y', $date);
            $userId = $request->get('id');

            $schedule = $this->schedules->getSchedule($userId, $date);

            if (empty($schedule)) { //No Schedules No Missions to show
                $type = 1;
                return response()->json(['type' => 1]);
            }

            //Schedules Table
            $duration = $this->schedules->getDuration($userId, $date);
            $first = $this->schedules->getFirstLogin($schedule);
            $last = $this->schedules->getLastLogout($schedule);

            //Missions Table
            $missions = $this->missions->getMissions($userId, $date);

            return response()->json([
                'missions' => $missions,
                'schedule' => $schedule,
                'first' => $first,
                'last' => $last,
                'duration' => $duration,
                'userId' => $schedule[count($schedule) - 1]->logout
            ]);
        } catch (Exception $exception) {
            return response()->json(['Data' => $exception->getMessage()]);
        }
    }

    /**
     * @param RateScoreRequest $request
     * request -> userId
     * request -> score points rate
     * @return JsonResponse
     */
    public function rateUser(RateScoreRequest $request)
    {
        try {
            $data['user_id'] = $request->get('userId');
            $data['rate'] = $request->get('score');

            $oldRate = $this->rates->getRateThisMonth($data['user_id'], now());
            $rate_id = empty($oldRate) ? 0 : $oldRate->id;

            if (auth()->user()->is_Admin)
                $this->rates->updateData($rate_id, $data);

            return response()->json(['data' => $data['rate']]);
        } catch (Exception $exception) {
            return response()->json(['Data' => $exception->getMessage()]);
        }

    }
}
