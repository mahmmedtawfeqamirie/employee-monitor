<?php

namespace App\Http\Controllers\Admin;

use App\Classes\FileHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\updateLoginRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Models\Users\Schedule;
use App\Models\Users\Section;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class DashboardController extends Controller
{
    protected $users, $sections, $schedules;

    /**
     * DashboardController constructor.
     * @param User $users
     * @param Section $sections
     * @param Schedule $schedules
     */
    public function __construct(User $users, Section $sections, Schedule $schedules)
    {
        $this->users = $users;
        $this->sections = $sections;
        $this->schedules = $schedules;

    }

    /**
     * send all users and current user
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function index()
    {
        $users = $this->users->GetDataWithoutAdminUsers();
        $current_user = Auth::user();
        return view('Dashboard.dashboard', compact(['users', 'current_user']));
    }

    /**
     * @param null $id
     * if id is null means you want to add user
     * else means you want to update user
     * send all sections and  user by id
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function addUserPage($id = null)
    {
        $user = $this->users->GetUserById($id);
        if (!empty($user))
            $this->authorize('update', $user);

        $sections = $this->sections->getData();
        return view('Dashboard.addUserForm', compact(['sections', 'user']));
    }

    /**
     * @param CreateUserRequest $request
     * if $image is default means you didn't upload image
     * if $section_id is admin means is_Admin = 1 and section_id is null
     * @return Application|RedirectResponse|Redirector|View
     */
    public function addUsers(CreateUserRequest $request)
    {
        $data = $request->only('name', 'password', 'section_id');
        $data['phone'] = $request->get('phone');

        $image = $request->file('profile_picture', 'default.jpg');
        $data['profile_picture'] = $this->uploadImage($image, null);

        $data['is_Admin'] = 0;
        if ($data['section_id'] == 'admin') {
            $data['is_Admin'] = 1;
            $data['section_id'] = null;
        }

        $this->users->createData($data);

        return redirect('/dashboard');
    }

    /**
     * @param UpdateUserRequest $request
     * if $id is 0 means create user or update user
     * if $image is default means you didn't upload image
     * if $section_id is admin means is_Admin = 1 and section_id is null
     * @return Application|RedirectResponse|Redirector|View
     */
    public function editUsers(UpdateUserRequest $request)
    {
        $data = $request->only('name');
        $data['phone'] = $request->get('phone');

        $id = $request->get('id');
        $user = $this->users->GetUserById($id);

        if ($request->filled('password'))
            $data['password'] = $request->get('password');

        $image = $request->file('profile_picture', 'default.jpg');
        $data['profile_picture'] = $this->uploadImage($image, $user);

        $section_id = $request->get('section_id');
        $data['section_id'] = $section_id == null ? $user->section_id : $section_id;

        if ($data['section_id'] == "admin") {
            $data['is_Admin'] = 1;
            $data['section_id'] = null;
        }

        $this->users->updateData($id, $data);

        return Auth::user()->is_Admin ? redirect('/dashboard') : redirect('/profile/' . $id);
    }

    /**
     * @param $id
     * $id represents userId
     * send first login for user by id
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function editLoginPage($id)
    {
        $schedule = $this->schedules->getFirstLoginUserToday($id);
        return view('Admins.updateLoginTime', compact('schedule'));
    }

    /**
     * @param updateLoginRequest $request
     * $id means scheduleId for selected user
     * $NewLogin means New Time will store in database for login
     * @return Application|RedirectResponse|Redirector|View
     */
    public function updateLogin(updateLoginRequest $request)
    {
        $id = $request->get('id');

        $date = date('Y-m-d', strtotime(now()));
        $time = date('H:i:s', strtotime($request->get('updLogin')));
        $data['login'] = date('Y-m-d H:i:s', strtotime("$date $time"));

        $this->schedules->updateData($id, $data);

        return redirect('dashboard');
    }

    /**
     * @param $image
     * if image is default means don't upload
     * @param $user
     * if $user is null means add image or update image
     * @return string|null
     */
    public function uploadImage($image, $user)
    {
        if (empty($user)) {         //Add Image

            $file = FileHelper::$defaultImage;  //Store Default Image

            if ($image != FileHelper::$defaultImage)
                $file = FileHelper::uploadFile($image); //Store New Image

            return $file;
        } else {    //Update Image

            if ($image != FileHelper::$defaultImage) {

                if ($user->profile_picture != FileHelper::$defaultImage) //Remove Old Image
                    FileHelper::deleteFile($user->profile_picture, 'picture/profile');

                return FileHelper::uploadFile($image);
            }

            return $user->profile_picture;
        }
    }
}
