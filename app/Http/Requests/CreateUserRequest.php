<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'phone' => $this->get('phone')
        ]);
    }

    public function rules()
    {
        return [
            'section_id' => 'required',
            'name' => 'required|regex:/^[A-Za-z\s]+$/',
            'phone' => 'nullable|regex:/^[0-9]+$/',
            'profile_picture' => 'sometimes|mimes:jpg,jpeg,png',
            'password' => 'required|string',
            'RentredNewPassword' => 'required|same:password'
        ];
    }

}
