<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'phone' => $this->get('phone')
        ]);
    }

    public function rules()
    {
        return [
            'id' => 'required|integer',
            'name' => 'required|string',
            'phone' => 'nullable|regex:/^[0-9]+$/',
            'profile_picture' => 'sometimes|mimes:jpg,jpeg,png',
        ];
    }
}
