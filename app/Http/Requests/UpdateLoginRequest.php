<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateLoginRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'updLogin' => 'required|before:oldLogin',
            'id' => 'required|integer'
        ];
    }
}
