<?php

namespace App\Http\Requests;

use App\Enums\MissionStatus;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class CreateMissionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'id' => $this->get('id')
        ]);
    }

    public function rules()
    {
        return [
            'user_id' => 'required|integer',
            'id' => 'nullable|integer',
            'description' => 'required|string',
            'status' => ['required', new EnumValue(MissionStatus::class)]
        ];
    }
}
