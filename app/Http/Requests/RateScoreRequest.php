<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RateScoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'userId' => 'required|integer',
            'score' => 'required|numeric'
        ];
    }
}
