<?php


namespace App\Models\Activity;


use App\Base\BaseModel;
use App\Models\User;

class Rating extends BaseModel
{
    protected $table = 'ratings';
    protected $fillable = [
        'rate',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getRateUser($userId)
    {
        return
            self::where('user_id', $userId)
                ->avg('rate');
    }

    public function getRateThisMonth($user_id, $date)
    {
        return
            self::where('user_id', $user_id)
                ->whereMonth('created_at', $date->format('m'))
                ->whereYear('created_at', $date->format('Y'))
                ->first();
    }

}
