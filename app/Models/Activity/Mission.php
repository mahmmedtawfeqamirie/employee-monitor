<?php


namespace App\Models\Activity;


use App\Base\BaseModel;
use App\Models\User;
use Carbon\Carbon;

class Mission extends BaseModel
{
    protected $table = 'missions';

    protected $fillable = [
        'description',
        'status',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getData($user_id, $date)
    {
        return
            self::where('user_id', $user_id)
                ->whereMonth('created_at', $date->format('m'))
                ->whereYear('created_at', $date->format('Y'))
                ->get();
    }

    public function getDataDoneThisMonthByUserId($user_id)
    {
        return
            self::where('user_id', $user_id)
                ->where('status', 'done')
                ->whereMonth('created_at', Carbon::now()->format('m'))
                ->get()
                ->count();
    }

    public function getMissions($userId, $date)
    {
        return
            self::where('user_id', $userId)
                ->whereDate('created_at', $date)
                ->get();
    }
}
