<?php


namespace App\Models\Activity;


use App\Base\BaseModel;
use App\Models\User;

class NonAttendence extends BaseModel
{
    protected $table = "non_attendences";
    protected $fillable = [
        'description',
        'user_id',
        'user_status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getDataByUserId($user_id, $date)
    {
        return
            self::where('user_id', $user_id)
                ->whereMonth('created_at', $date->format('m'))
                ->whereYear('created_at', $date->format('Y'))
                ->get();
    }

    public function getAbsent($userId)
    {
        return
            self::where('user_id', $userId)
                ->where('description', 'waiting response...')
                ->get();
    }

    public function updateAbsent($userId, $data)
    {
        return
            self::where('user_id', $userId)
                ->update($data);
    }

}
