<?php

namespace App\Models;

use App\Models\Activity\Rating;
use App\Models\Users\Section;
use App\traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable, ModelTrait;

    protected $table = 'users';

    protected $fillable = [
        'name',
        'password',
        'is_Admin',
        'phone',
        'profile_picture',
        'section_id',
        'status'
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = Hash::make($password);
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

    public function rates()
    {
        return $this->hasMany(Rating::class, 'user_id');
    }

    public function GetDataWithoutAdminUsers()
    {
        return
            self::where('is_Admin', 0)
                ->with('section')
                ->get();
    }

    public function GetUserById($id)
    {
        return
            self::find(['id' => $id])
                ->first();
    }

    public function authentication($data)
    {
        if (auth()->attempt(['id' => $data['id'], 'password' => $data['password']]))
            return true;
        return false;
    }

    public function getObjectUser($userId)
    {
        return
            self::with('section')
                ->find($userId)
                ->first();
    }

}

