<?php


namespace App\Models\Users;


use App\Base\BaseModel;
use App\Models\User;
use Illuminate\Support\Carbon;

class Schedule extends BaseModel
{
    protected $table = "schedules";

    protected $fillable = [
        'login',
        'logout',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getWorkTime($userId)
    {
        $totalWorkTime = self::getDuration($userId, now());
        $totalWorkTime = self::convertToSecond($totalWorkTime);
        return floor($totalWorkTime / 3600);
    }

    public function isLoggedInToday($userId)
    {
        $schedule =
            self::where('user_id', $userId)
                ->whereDate('login', now())
                ->orderBy('login', 'DESC')
                ->first();

        if (empty($schedule))
            return false;

        $firstLogin = self::getFirstLogin($schedule);

        $lastLogout = $schedule->logout;

        if (empty($firstLogin))
            return false;

        if (empty($lastLogout))
            return true;
        else
            return false;
    }

    public function parseTimeFormat($time)
    {
        return Carbon::parse($time)->format('H:i:s');
    }

    public function getFirstLogin($schedule)
    {
        return self::parseTimeFormat($schedule->first()->login);
    }

    public function getFirstLoginUserToday($id)
    {
        return
            self::where('user_id', $id)
                ->whereDate('login', now())
                ->with('user')
                ->first();
    }

    public function checkLogoutStatus($userId)
    {
        $userLogout =
            self::where('user_id', $userId)
                ->orderBy('login', 'DESC')
                ->first('logout');

        if (empty($userLogout->logout))
            return false;
        return true;
    }

    public function saveUserLogoutTime($userId)
    {
        $schedules =
            self::where('user_id', $userId)
                ->whereDay('login', now())
                ->orderBy('login', 'DESC')
                ->first();
        if (empty($schedules))
            return false;

        $schedules->update(['logout' => Carbon::now()->toDateTime()]);
    }

    public function getSchedule($userId, $date)
    {

        $schedule =
            self::where('user_id', $userId)
                ->whereDate('login', $date)
                ->get();

        if ($schedule->isEmpty())
            return null;

        $schedule = $this->getScheduleFormat($schedule);
        return $schedule;
    }

    public function getScheduleFormat($schedules)
    {
        foreach ($schedules as $schedule) {
            if (empty($schedule->login)) continue;

            $schedule->login = self::parseTimeFormat($schedule->login);

            if (empty($schedule->logout)) continue;

            $schedule->logout = self::parseTimeFormat($schedule->logout);
        }
        return $schedules;
    }

    public function getLastLogout($schedule)
    {
        if (empty($schedule[count($schedule) - 1]->logout))
            return null;
        return $schedule[count($schedule) - 1]->logout;
    }

    public function getDuration($userId, $date)
    {
        $sum = 0;

        $schedules = self::getSchedule($userId, $date);
        $schedules = empty($schedules) ? [] : $schedules;

        foreach ($schedules as $schedule) {
            if (empty($schedule->logout))
                $schedule->logout = Carbon::now();

            $logout = strtotime($schedule->logout);

            $login = strtotime($schedule->login);
            $sum += ($logout - $login);

        }
        return gmdate("H:i:s", $sum);
    }

    public function convertToSecond($totalWorkTime)
    {
        preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $totalWorkTime);

        sscanf($totalWorkTime, "%d:%d:%d", $hours, $minutes, $seconds);

        $totalWorkTime = $hours * 3600 + $minutes * 60 + $seconds;
        return $totalWorkTime;
    }

    public function getLastLogin($userId)
    {
        return
            self::where('user_id', $userId)
                ->whereDay('login', '!=', now())
                ->orderBy('login', 'desc')
                ->first();
    }

    public function updateForgetLogout($login, $data)
    {
        return
            self::where('login', $login)
                ->where('login', '!=', now())
                ->update($data);
    }

}
