<?php


namespace App\Models\Users;


use App\Base\BaseModel;
use App\Models\User;

class Section extends BaseModel
{
    protected $table = 'sections';

    protected $fillable = [
        'name',
        'Image'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'user_id');
    }

    public function getData()
    {
        return self::all();
    }
}
