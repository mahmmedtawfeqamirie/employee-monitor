<?php


namespace App\Classes;


use Exception;
use Illuminate\Support\Facades\File;

class FileHelper
{

    public static $defaultImage = "default.jpg";

    public static function uploadFile($file)
    {
        try {
            $extension = $file->extension();
            $storageType = 'Picture\Profile';
            $filename = StringHelper::getFileName($extension);
            $shortPath = StringHelper::getShortPath($storageType);
            $path = StringHelper::getFullPath($shortPath);
            $file->move($path, $filename);
            return $filename;
        } catch (Exception $e) {
            return null;
        }

    }

    public static function checkExistFile($path)
    {
        return File::exists(public_path() . $path);
    }

    public static function deleteFile($filename, $folders)
    {
        $path = StringHelper::getShortPath($folders) . $filename;

        if (FileHelper::checkExistFile($path))
            return File::delete(public_path($path));
        return null;
    }
}
