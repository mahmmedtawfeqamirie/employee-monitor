<?php


namespace App\Classes;


class StringHelper
{

    public static $defaultUserImage = '/media/picture/profile/11589406136.png';

    public static function getShortPath($storageType)
    {
        return '/media/' . $storageType . '/';
    }

    public static function getFileName($fileExtension)
    {
        return time() . '.' . $fileExtension;
    }

    public static function getFullPath($shortPath)
    {
        return public_path() . $shortPath;
    }

}
