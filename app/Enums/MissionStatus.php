<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class MissionStatus extends Enum
{
    const pending = "pending";
    const progress = "progress";
    const done = "done";
}
