<?php

namespace App\Console\Commands;

use App\Models\Activity\NonAttendence;
use App\Models\User;
use App\Models\Users\Schedule;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class Attendences extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:attendences';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $users = User::where('is_Admin', '!=', 1)->get();

        if (empty($users))
            $users = [];

        foreach ($users as $user) {

            $attendence = new NonAttendence();

            $schedule =
                Schedule::where('user_id', $user->id)
                    ->whereDate('login', Carbon::yesterday())
                    ->first();

            if (empty($schedule))
                $attendence->createData(['user_id' => $user->id]);

        }

    }
}
