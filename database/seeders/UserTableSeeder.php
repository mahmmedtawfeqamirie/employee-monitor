<?php

namespace Database\Seeders;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(5)->create();
//        DB::table('users')->insert([
//            'name' => 'admin',
//            'password' => Hash::make('password'),
//            'is_Admin' => 1,
//            'profile_picture' => 'default.jpg'
//        ]);
    }
}
