<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->insert
        ([
            ['name' => 'BackEnd Developer', 'image' => 'Web Developer.jpg'],
            ['name' => 'FrontEnd Developer', 'image' => 'FrontEnd-Developer.png'],
            ['name' => 'Testing User', 'image' => 'Testing-User.jpg'],
            ['name' => 'Android Developer', 'image' => 'Android-Developer.jpg']
        ]);
    }
}
