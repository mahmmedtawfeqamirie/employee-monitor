<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_id')->nullable();
            $table->string('name');
            $table->integer('is_Admin');
            $table->integer('status')->default(0);
            $table->string('phone')->nullable();
            $table->string('password');
            $table->string('profile_picture')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        DB::statement("SET foreign_key_checks=0");
        Schema::dropIfExists('users');
        DB::statement("SET foreign_key_checks=1");
    }
}
