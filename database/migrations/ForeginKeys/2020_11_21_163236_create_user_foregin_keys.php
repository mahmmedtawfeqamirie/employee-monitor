<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserForeginKeys extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_section_id_foreign');
            $table->dropColumn('section_id');
        });

    }
}
