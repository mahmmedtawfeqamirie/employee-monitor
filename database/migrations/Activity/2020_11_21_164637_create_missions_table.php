<?php

use App\Enums\MissionStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{

    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('description');
            $table->enum('status', MissionStatus::getValues());
            $table->timestamps();
        });
    }

    public function down()
    {
        DB::statement("SET foreign_key_checks=0");
        Schema::dropIfExists('missions');
        DB::statement("SET foreign_key_checks=1");
    }
}
