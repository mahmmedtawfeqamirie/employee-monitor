<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingsTable extends Migration
{
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->double('rate');
            $table->timestamps();
        });
    }

    public function down()
    {
        DB::statement("SET foreign_key_checks=0");
        Schema::dropIfExists('ratings');
        DB::statement("SET foreign_key_checks=1");
    }
}
