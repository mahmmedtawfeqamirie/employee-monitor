<?php

use App\Http\Controllers\Users\ActivityController;
use App\Http\Controllers\Users\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/add', [\App\Http\Controllers\Users\AuthController::class, 'addUsers']);

Route::post('date/activity', [\App\Http\Controllers\Users\ProfileController::class, 'updateMonthlyMissions']);

Route::post('mission', [\App\Http\Controllers\Users\ProfileController::class, 'createMission']);

