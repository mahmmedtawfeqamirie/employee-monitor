<?php

use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Support\Facades\Route;

Route::get('dashboard', [DashboardController::class, 'index']);
Route::post('dashboard/uploadUser', [DashboardController::class, 'addUsers'])->name('uploadUser');

Route::get('dashboard/addusers', [DashboardController::class, 'addUserPage'])->name('AddUserPage');

Route::get('dashboard/user/editlogin/{id}', [DashboardController::class, 'editLoginPage']);

Route::post('dashboard/saveupdatelogin', [DashboardController::class, 'updateLogin']);
