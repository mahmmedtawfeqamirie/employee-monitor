<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Users\ActivityController;
use App\Http\Controllers\Users\ProfileController;
use Illuminate\Support\Facades\Route;


Route::middleware(['AuthEmployee'])->group(function () {
    Route::get('dashboard/user/{id}', [DashboardController::class, 'addUserPage']);
    Route::post('dashboard/editUser', [DashboardController::class, 'editUsers']);

    Route::get('profile/{userId}', [ProfileController::class, 'showProfile'])->name('profile');
    Route::get('/activity/{userId}', [ActivityController::class, 'showactivity'])->name('activity');
    Route::get('missUpdate', [ActivityController::class, 'missUpdate'])->name('missUpdate');
    Route::get('rateUser', [ActivityController::class, 'rateUser'])->name('rateUser');


    Route::post('profile/createMission', [ProfileController::class, 'createMission']);
    Route::get('profile/monthlyMissions/{id}', [ProfileController::class, 'monthlyMissions']);
    Route::get('updateMissionsPage', [ProfileController::class, 'updateMonthlyMissions']);
    Route::get('addMission', [ProfileController::class, 'createMission']);
});
Route::get('profile/{userId}', [ProfileController::class, 'showProfile'])->name('profile');

Route::post('saveNotAttendReason', [ProfileController::class, 'saveNotAttendReason'])->name('saveNotAttendReason');

Route::post('saveUpdateLogout', [ProfileController::class, 'saveUpdateLogout'])->name('saveUpdateLogout');
