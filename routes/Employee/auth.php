<?php

use App\Http\Controllers\Users\AuthController;
use Illuminate\Support\Facades\Route;


Route::get('/', [AuthController::class, 'welcome'])->name('welcome');

Route::get('/login', [AuthController::class, 'viewLogin'])->name('login');

Route::post('passwordLogin', [AuthController::class, 'login'])->name('loggin');

Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
