<a class="btn btn-sm btn-dual mr-2" href="{{url('dashboard')}}">
    <span>All Users</span>
    <i class="fas fa-users"></i>
</a>
<a class="btn btn-sm btn-dual mr-2" href="{{route('AddUserPage')}}">
    <span>Add Users</span>
    <i class="fas fa-user-plus"></i>
</a>
