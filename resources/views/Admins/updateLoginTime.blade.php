<!DOCTYPE html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">
</head>
<body>
<!-- Page Container -->
<!--

<div id="page-container">

-->
<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    {{--    <div class="bg-image" style="background-image: url('assets/media/photos/photo34@2x.jpg');">--}}
    <div class="bg-image">
        <div class="hero-static bg-black-50">
            <div class="content">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <!-- Unlock Block -->
                        <div class="block block-themed mb-0">
                            <div class="block-header bg-black">
                                <h3 class="block-title">Update Login</h3>
                                <div class="block-options">
                                    <!--nothing-->
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="p-sm-3 px-lg-4  text-center">
                                    @if(!empty($schedule))
                                        <img class="img-avatar img-avatar96"
                                             src="{{asset("media/picture/profile/".$schedule->user->profile_picture)}}"
                                             alt="">
                                        <p class="font-w600 my-2">
                                            {{$schedule->user->name}}
                                        </p>

                                        <!-- Unlock Form -->
                                        <form id="updateLoginForm" action="{{ url('dashboard/saveupdatelogin') }}"
                                              method="post">
                                            @csrf
                                            <div class="form-group py-1">
                                                <label> First Login Today</label>
                                                <input type="text" readonly
                                                       class="form-control form-control-lg form-control-alt text-center"
                                                       name="oldLogin"
                                                       id="oldLogin" value="{{$schedule->login}}">
                                            </div>
                                            <div class="form-group py-1">
                                                <label>Please Enter New Login Time</label>
                                                <input type="time"
                                                       class="form-control form-control-lg form-control-alt text-center"
                                                       id="updLogin" name="updLogin" placeholder="set time">
                                                <span class="text-danger">{{ $errors->first('updLogin') }}</span>
                                            </div>
                                            <div class="form-group row justify-content-center">
                                                <div class="col-md-6 col-xl-5">
                                                    <button type="submit" class="btn btn-block btn-light">
                                                        <i class="fa fa-fw fa-check mr-1"></i> Submit
                                                    </button>
                                                </div>
                                            </div>
                                            <input type="number" hidden value="{{$schedule->id}}" name="id"/>
                                            @else
                                                <span
                                                    class="text-danger">User didn't login today</span>
                                            @endif
                                        </form>
                                        <!-- END Unlock Form -->
                                </div>
                            </div>
                        </div>
                        <!-- END Unlock Block -->
                    </div>
                </div>
            </div>
        </div>
        @include('Partial.footer')
    </div>
    <!-- END Page Content -->
</main>
<!-- END Main Container -->
<!-- END Page Container -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--template-->

<script src="{{asset('js/core/jquery.min.js')}}"></script>

<script src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('js/core/simplebar.min.js')}}"></script>

<script src="{{asset('js/core/jquery-scrollLock.min.js')}}"></script>

<script src="{{asset('js/core/jquery.appear.min.js')}}"></script>

<script src="{{asset('js/core/js.cookie.min.js')}}"></script>

<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>


<script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

{{--<script src="{{asset('js/Admins/updloginTime.js')}}"></script>--}}

<script src="{{asset('js/pages/op_auth_lock.min.js')}}"></script>

<script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('js/plugins/jquery-validation/additional-methods.min.js')}}"></script>

</body>
</html>

