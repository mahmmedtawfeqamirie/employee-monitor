@extends('Partial.mainView')

@section('content')
    <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-4">
                <!-- Sign Up Block -->
                <div class="block block-themed block-fx-shadow mb-0">
                    <div class="block-header bg-success"></div>
                    <div class="js-wizard-validation block block">
                        <!-- Step Tabs -->
                        <ul class="nav nav-tabs nav-tabs-block nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#wizard-validation-step1" data-toggle="tab">1.
                                    Information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#wizard-validation-step2" data-toggle="tab">2. Picture</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#wizard-validation-step3" data-toggle="tab">3. Password</a>
                            </li>
                        </ul>
                        <!-- END Step Tabs -->

                        <!-- Form -->
                        <form class="js-wizard-validation-form" id="AddNewUser" enctype="multipart/form-data"
                              @if(empty($user))
                              action="{{ url('dashboard/uploadUser') }}"
                              @else
                              action="{{ url('dashboard/editUser') }}"
                              @endif
                              method="post">
                            <!-- Wizard Progress Bar -->
                            @csrf
                            <div class="block-content block-content-sm">
                                <div class="progress" data-wizard="progress" style="height: 8px;">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                         role="progressbar" style="width: 30%;" aria-valuenow="30" aria-valuemin="0"
                                         aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!-- END Wizard Progress Bar -->
                            <!-- Steps Content -->
                            <div class="block-content block-content-full tab-content px-md-5"
                                 style="min-height: 300px;">
                                <!-- Step 1 -->
                                <div class="tab-pane active" id="wizard-validation-step1" role="tabpanel">
                                    <div class="form-group">
                                        <label for="NewUserName">Full Name</label>
                                        <input class="form-control" type="text" id="NewUserName" name="name"
                                               @if( !empty($user) )
                                               value="{{$user->name}}"
                                            @endif
                                        >
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewUserPhone">Phone</label>
                                        <input class="form-control" type="text" id="NewUserPhone" name="phone"
                                               @if( !empty($user) )
                                               value="{{$user->phone}}"
                                            @endif
                                        >
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    </div>
                                    @if(auth()->user()->is_Admin)
                                        <div class="form-group">
                                            <label for="NewUserSection">Section</label>
                                            <select class="form-control"
                                                    id="NewUserSection" name="section_id">

                                                @if(empty($user))
                                                    <option value="" selected hidden>Please select a Section</option>
                                                @endif

                                                @if(!empty($sections))
                                                    @foreach($sections as $section)

                                                        @if( !empty($user) &&  $section->id == $user->section_id )
                                                            <option selected
                                                                    value="{{$section->id}}">{{$section->name}}</option>
                                                        @else
                                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                                        @endif

                                                    @endforeach
                                                @endif
                                                <option value="admin"
                                                        @if(!empty($user) && $user->is_Admin )
                                                        selected
                                                    @endif
                                                >
                                                    Admin Role
                                                </option>
                                            </select>
                                            <span class="text-danger">{{ $errors->first('section_id') }}</span>
                                        </div>
                                    @endif
                                </div>
                                <!-- END Step 1 -->

                                <!-- Step 2 -->
                                <div class="tab-pane row" id="wizard-validation-step2" role="tabpanel">
                                    <div class="profile-header-img col-md-12">
                                        <img class="rounded-circle img-avatar img-avatar96" id="imageContainer"
                                             @if(empty($user))
                                             src="{{asset("media/picture/profile/default.jpg")}}"
                                             @else
                                             src="{{asset("media/picture/profile/".$user->profile_picture)}}"
                                            @endif
                                        />
                                        <!-- badge -->
                                        <div class="rank-label-container">
                                            <span class="label label-default rank-label"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewUserPicture">Profile Photo</label><br>
                                        <input id="NewUserPicture" type="file" name="profile_picture" accept="image/*"/>
                                        <small id="fileHelp" class="form-text text-muted">Please upload a valid image
                                            file. Size of image should not be more than 2MB. </small>
                                    </div>
                                </div>
                                <!-- END Step 2 -->

                                <!-- Step 3 -->
                                <div class="tab-pane" id="wizard-validation-step3" role="tabpanel">
                                    <div class="form-group py-2">
                                        <label for="loginPassword">Enter Your New Password</label>
                                        <input type="password" class="form-control form-control-alt form-control-lg"
                                               id="NewPassword" name="password" placeholder="Password">
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    </div>
                                    <div class="form-group py-2">
                                        <label for="loginPassword">Renter Your New Password</label>
                                        <input type="password" class="form-control form-control-alt form-control-lg"
                                               id="RentredNewPassword" name="RentredNewPassword" placeholder="Password">
                                        <span class="text-danger">{{ $errors->first('RentredNewPassword') }}</span>
                                    </div>
                                </div>
                                <!-- END Step 3 -->
                            </div>
                            @if(!empty($user))
                                <input type="number" value="{{$user->id}}" name="id" hidden>
                        @endif
                        <!-- END Steps Content -->
                            <!-- Steps Navigation -->
                            <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="button" class="btn btn-secondary" data-wizard="prev">
                                            <i class="fa fa-angle-left mr-1"></i> Previous
                                        </button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <button type="button" class="btn btn-secondary" id="Validator"
                                                data-wizard="next">
                                            Next <i class="fa fa-angle-right ml-1"></i>
                                        </button>
                                        <button type="submit" class="btn btn-primary d-none" data-wizard="finish">
                                            <i class="fa fa-check mr-1"></i> Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- END Steps Navigation -->

                        </form>
                        <!-- END Form -->
                    </div>
                    <!-- END Validation Wizard Classic -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

    <script src="{{asset('js/plugins/jquery-validation/additional-methods.min.js')}}"></script>

    <script>
        $(document).ready(() => {
            var btn = document.getElementById('addMissionBtn');
            btn.style.display = 'none';
        });
    </script>

    {{--    <script src="{{asset('js/Users/AddNewUserValidation.js')}}"></script>--}}

    {{--    <script src="{{asset('js/Users/AddNewUser.js')}}"></script>--}}

    <script src="{{asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.min.js')}}"></script>

    <script src="{{asset('js/pages/be_forms_wizard.min.js')}}"></script>

@endsection

@section('links')
@endsection
