@extends('Partial.mainView')
@section('content')
    <div class="content">
        <div class="row items-push">
            @foreach($users as $user)
                <div class="col-md-4 invisible " data-toggle="appear" data-class="animated zoomIn">
                    <!-- Team Member -->
                    <div class="block border border-dark">
                        <div class="block-header">
                            <div class="block-options col-md-12">
                                <h2 class="block-title text-center">
                                    {{$user->name}}</h2>
                            </div>
                        </div>
                        <div class="block-header">
                            <div class="block-options id-saver col-md-12 text-center" data-id="{{$user->id}}">
                                <a class="btn btn-sm btn-light ViewProfileBtn" href="{{url('/profile/'.$user->id)}}">
                                    <i class="fa fa-info text-primary mr-1"></i> View Profile
                                </a>
                                <a class="btn btn-sm btn-light editEmployeeBtn"
                                   href="{{url('/dashboard/user/'.$user->id)}}">
                                    <i class="fa fa-user-edit text-info mr-1"></i> Edit Profile
                                </a>
                                <a class="btn btn-sm btn-light editEmployeeTimeBtn"
                                   href="{{url('/dashboard/user/editlogin/'.$user->id)}}">
                                    <i class="fa fa-calendar-check text-info mr-1"></i> Edit login
                                </a>
                            </div>

                        </div>
                        <div class="block-content block-content-full text-center bg-image"
                             style="background-image: url('{{asset("media/picture/sections/".$user->section->image)}}');">
                            <img class="img-avatar img-avatar96 img-avatar-thumb"
                                 src="{{asset("media/picture/profile/".$user->profile_picture)}}"
                                 alt="">
                        </div>
                        <div class="block-content font-size-sm col-md-12 ">
                            <h3 class="text-center">{{$user->section->name}}</h3>
                        </div>
                    </div>
                    <!-- END Team Member -->
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/Users/UsersRouting.js')}}"></script>
    <script>
        $(document).ready(() => {
            var btn = document.getElementById('addMissionBtn');
            btn.style.display = 'none';
        });
    </script>
@endsection

