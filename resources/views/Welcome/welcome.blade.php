<!doctype html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">

    <!-- END Stylesheets -->
</head>
<body>
<!-- Page Container -->
<!--

-->
<div id="page-container" class="main-content-boxed">

    <!-- Main Container -->
    <main id="main-container">

        <!-- Hero -->
        <div class="bg-image" style="background-image: url('https://img.freepik.com/free-vector/illustration-web-design_53876-18157.jpg?size=626&ext=jpg&ga=GA1.2.1614264220.1617148800');">
            <div class="hero bg-black-75 overflow-hidden">
                <div class="hero-inner">
                    <div class="content content-full text-center">
                        <div class="mb-5 invisible" data-toggle="appear" data-class="animated fadeInDown">
{{--                            <a><img src="https://www.404developers.com/images/logo5.svg" alt="" width="400px"--}}
{{--                                    height="100px"/></a>--}}
                        </div>

                        <h2 class="h3 font-w400 text-white-50 mb-5 invisible text-center" data-toggle="appear"
                            data-class="animated fadeInDown" data-timeout="300">
                            Welcome To Developers Attendees And Mission Record System
                        </h2>
                        <span class="m-2 d-inline-block invisible" data-toggle="appear" data-class="animated fadeInUp"
                              data-timeout="600">
                                    <a class="btn btn-primary px-4 py-2" data-toggle="click-ripple"
                                       href="{{route('login')}}">
                                        <i class="fa fa-fw fa-rocket mr-1"></i> Get Started
                                    </a>
                                </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- footer -->

        <!-- END Footer -->

    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--template-->

<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>

</body>
</html>
