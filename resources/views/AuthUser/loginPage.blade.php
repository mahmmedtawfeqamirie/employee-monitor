<!DOCTYPE html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">

    <link rel="stylesheet" href="{{asset('js/IziToast/css/iziToast.min.css')}}">

</head>

<body>
<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div class="hero-static">
    <!--<div class="bg-image" style="background-image: url({{asset('media/Picture/Cover/photo34@2x.jpg')}});">-->
        <div class="row justify-content-center">
            <div class="col-md-4">
                <!-- Sign In Block -->
                <br>
                <div class="block block-themed block-fx-shadow mb-0">
                    <div class="block-header">
                        <h3 class="block-title">Sign In</h3>
                        <div class="block-options">
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="p-sm-3 px-lg-4 py-lg-5">
{{--                            <a><img src="https://www.404developers.com/images/logo5.svg" alt="" width="350px"--}}
{{--                                    height="100px"/></a>--}}
                            <p>Welcome, please login</p>
                        </div>
                        <!-- Sign In Form -->
                        <div class="js-wizard-simple block block">
                            <!-- Step Tabs -->
                            <ul class="nav nav-tabs nav-tabs-alt nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#wizard-simple2-step1" data-toggle="tab"> Login</a>
                                </li>
                            </ul>
                            <!-- END Step Tabs -->

                            <!-- Form -->
                            <form>
                            @csrf

                            <!-- Step 2 -->
                                <div class="tab-pane" id="wizard-simple2-step2" role="tabpanel">
                                    <div class="form-group py-2">
                                        <label for="loginId">Enter Your Id</label>
                                        <input type="number" class="form-control form-control-alt form-control-lg"
                                               id="loginId" name="loginId" placeholder="Id" min="1">
                                    </div>
                                    <div class="form-group py-2">
                                        <label for="loginPassword">Enter Your Password</label>
                                        <input type="password" class="form-control form-control-alt form-control-lg"
                                               id="loginPassword" name="loginPassword" placeholder="Password">
                                    </div>
                                    <div class="form-group row py-2">
                                        <div class="col-md-6 col-xl-5">
                                            <button type="button" id="loginBtn" onclick="pass_login()"
                                                    class="btn btn-block btn-primary">
                                                <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Log In
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END Form -->
                        </div>
                        <!-- END Simple Wizard 2 -->
                    </div>

                    <!-- END Sign In Form -->
                    <div class="content content-full font-size-sm text-muted text-center">
                        <input type="hidden" name="_token" id="Login_token" value="{{csrf_token()}}">
{{--                        <strong>404 Dev @ 2019</strong>--}}
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Sign In Block -->
    </div>
    </div>
    </div>
    <!-- END Page Content -->
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>

<script src="{{asset('js/Users/Users_login.js')}}"></script>

<script src="{{asset('js/Users/instascan.min.js')}}"></script>

<script src="{{asset('js/IziToast/js/iziToast.min.js')}}"></script>
<!-- END Main Container -->
</body>


