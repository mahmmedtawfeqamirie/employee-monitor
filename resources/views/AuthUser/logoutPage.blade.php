
<!doctype html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/amethyst.min.css"> -->
    <!-- END Stylesheets -->
</head>
<body>
<!-- Page Container -->
<!--

-->
<div id="page-container" class="main-content-boxed">

    <!-- Main Container -->
    <main id="main-container">

        <!-- Hero -->
        <div class="bg-image" style="background-image: url('{{asset('media/Picture/Pages/Main View.jpg')}}');">
            <div class="hero bg-black-75 overflow-hidden">
                <div class="hero-inner">
                    <div class="content content-full text-center">
                        <div class="mb-5 invisible" data-toggle="appear" data-class="animated fadeInDown">
{{--                            <a><img src="https://www.404developers.com/images/logo5.svg" alt="" width="400px" height="100px" /></a>--}}
                        </div>

                        <h2 class="h3 font-w400 text-white-50 mb-5 invisible text-center" data-toggle="appear" data-class="animated fadeInDown" data-timeout="300">
                            Good Bye 😢 We Hope To See You Soon
                        </h2>
                        <span class="m-2 d-inline-block invisible" data-toggle="appear" data-class="animated fadeInUp" data-timeout="600">
                                    <a class="btn btn-primary px-4 py-2" data-toggle="click-ripple" href="{{route('login')}}">
                                        <i class="fa fa-fw fa-backward mr-1"></i> Login Again 😀
                                    </a>
                                </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Hero -->

      <!-- footer -->

        <!-- END Footer -->

    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<!--
    OneUI JS Core

    Vital libraries and plugins used in all pages. You can choose to not include this file if you would like
    to handle those dependencies through webpack. Please check out assets/_es6/main/bootstrap.js for more info.

    If you like, you could also include them separately directly from the assets/js/core folder in the following
    order. That can come in handy if you would like to include a few of them (eg jQuery) from a CDN.

    assets/js/core/jquery.min.js
    assets/js/core/bootstrap.bundle.min.js
    assets/js/core/simplebar.min.js
    assets/js/core/jquery-scrollLock.min.js
    assets/js/core/jquery.appear.min.js
    assets/js/core/js.cookie.min.js
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--template-->


<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>

<!--
    OneUI JS

    Custom functionality including Blocks/Layout API as well as other vital and optional helpers
    webpack is putting everything together at assets/_es6/main/app.js
-->
</body>
</html>
