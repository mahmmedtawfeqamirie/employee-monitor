@extends('Partial.mainView')

@section('links')
    <link rel="stylesheet" href="{{asset('js/plugins/datatables/dataTables.bootstrap4.css')}}">

    <link rel="stylesheet" href="{{asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">


@section('scripts')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(() => {
            var btn = document.getElementById('addMissionBtn');
            btn.style.display = 'block';
        });
    </script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/Employees/monthMission.js')}}"></script>


    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>jQuery(function () {
            One.helpers(['table-tools-sections', 'datepicker']);
        });</script>

    <script src="{{asset('js/pages/be_tables_datatables.min.js')}}"></script>

    <script src="{{asset('js/Employees/Missions.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


@endsection

@section('content')

    <div class="block">
        <div class="block-content">
            <div class="row form-group">
                <div class="col-md-2">
                    <a class="btn btn-sm btn-primary push" href="{{route('profile',$userId)}}">
                        Back To Profile
                        <i class="fa fa-fw fa-backward mr-1"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="example-datepicker1">Select Day</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar-alt"></i>
                                                    </span>
                        </div>
                        <input readonly type="text" onchange="timeChange()"
                               class="js-datepicker form-control text-center"
                               id="mission_Datepicker" name="Mission_Datepicker" data-week-start="1"
                               data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy"
                               placeholder="{{\Carbon\Carbon::now()->format('d/m/Y')}}"
                               value="{{\Carbon\Carbon::now()->format('d/m/Y')}}">
                    </div>
                </div>
            </div>
            @include('Partial.tableSection',compact(['headTableSection','schedule','first','last','duration']))
            @include('Partial.normalTable',compact(['headNormalTable','missions']))
        </div>
    </div>
    <input type="hidden" class="id-saver" data-id="{{$userId}}" id="user_id" name="user_id" value="{{$userId}}">
@endsection
@include('Partial.model')


