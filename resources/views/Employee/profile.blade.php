@extends('Partial.mainView')

@section('content')
    <div class="bg-image" style="background-image: url('{{asset("media/picture/sections/".$user->section->image)}}');">
        <div class="bg-image">
            <div class="bg-black-50">
                <div class="content content-full text-center">
                    <div>
                        <img class="img-avatar img-avatar-thumb"
                             src="{{asset("media/picture/profile/".$user->profile_picture)}}">
                    </div>
                    <h1 class="h3 text-white mb-0">{{$user->name}}</h1>
                    <span class="text-white-75">{{$user->section->name}}</span>
                </div>
            </div>
        </div>
        <!-- END Hero -->

        <!-- Stats -->
        <div class="bg-white border-bottom">
            <div class="content content-boxed">
                <div class="row items-push text-center">
                    <div class="col-md-4">
                        <div class="font-size-sm font-w600 text-muted text-uppercase">Work Time</div>
                        <a class="link-fx font-size-h3" href="{{route('activity',$user->id)}}">{{$workTime}}H+</a>
                    </div>
                    <div class="col-md-4">
                        <div class="font-size-sm font-w600 text-muted text-uppercase">Mission Done</div>
                        <a class="link-fx font-size-h3"
                           href="{{url('profile/monthlyMissions/'.$user->id)}}"><span
                                id="MissionsCount">{{$missionsCount}}</span></a>
                    </div>
                    <div class="col-md-4">
                        <div class="font-size-sm font-w600 text-muted text-uppercase mb-2">Rating</div>
                        <div class="js-rating" id="EmployeeRate"
                             @if(!(auth()->user()->is_Admin))
                             data-read-Only="true"
                             @endif
                             data-half="true" data-score="{{$rate}}">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Stats -->
    <input hidden id="hidden-id" class="id-saver" data-id="{{$user->id}}" value="{{$user->id}}">
    <input hidden id="user_id" class="id-saver" data-id="{{$user->id}}" value="{{$user->id}}">
@endsection
@include('Partial.model')
@section('scripts')
    <script src="{{asset('js/Users/UserRating.js')}}"></script>
    <!-- Page JS Plugins -->plugins
    <script src="{{asset('js/raty-js/jquery.raty.js')}}"></script>
    <!-- Page JS Code -->
    <script src="{{asset('js/pages/be_comp_rating.min.js')}}"></script>

    <script src="{{asset('js/Employees/Missions.js')}}"></script>

    <script src="{{asset('js/Employees/monthMission.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        $(document).ready(() => {
            var btn = document.getElementById('addMissionBtn');
            btn.style.display = 'block';
        });
    </script>
@endsection
