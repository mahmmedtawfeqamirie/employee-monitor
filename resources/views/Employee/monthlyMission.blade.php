@extends('Partial.mainView')

@section('links')
    <link rel="stylesheet" href="{{asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}">
@endsection

@section('content')
    <div class="block">
        <div class="block-content">
            <div class="row form-group">
                <div class="col-md-2">
                    <a class="btn btn-sm btn-primary push" href="{{url('profile/'.$user_id)}}">
                        Back To Profile
                        <i class="fa fa-fw fa-backward mr-1"></i>
                    </a>
                </div>
                <div class="pt-2 col-md-2 offset-6">
                    <label for="example-datepicker1">Select Month</label>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="far fa-calendar-alt"></i>
                                                    </span>
                            </div>
                            <input readonly type="text" onchange="monthChange()"
                                   class="js-datepicker form-control text-center"
                                   id="monthMissionDatepicker" name="monthMissionDatepicker" data-week-start="1"
                                   data-autoclose="true" data-date-format="mm/yyyy"
                                   value="{{\Carbon\Carbon::now()->format('m/Y')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="block-content">
                    <div class="row form-group">
                        <div class="col-md-12">
                            {{--  Missions Table --}}
                            @php($index=1)
                            @include('Partial.dataTable',compact(['data','headDataTable','index']))
                        </div>
                    </div>
                </div>
            </div>
            @if(auth()->user()->is_Admin)
                <div class="block">
                    <div class="block-content">
                        <div class="row form-group">
                            <div class="col-md-12">
                                @php ($index=2)
                                @include('Partial.dataTable',compact(['data','headDataTable','index']))
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <input type="hidden" class="id-saver" id="user_id" data-id="{{$user_id}}" name="user_Id"
                   value="{{$user_id}}">
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(() => {
            var btn = document.getElementById('addMissionBtn');
            btn.style.display = 'block';
        });
    </script>
    <script src="{{asset('js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

    <script>jQuery(function () {
            One.helpers(['datepicker']);
        });</script>

    <script src="{{asset('js/Employees/monthMission.js')}}"></script>

    <script src="{{asset('js/Employees/Missions.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

@endsection

@include('Partial.model')
