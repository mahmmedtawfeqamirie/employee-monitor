<!DOCTYPE html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">

    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body>
<!-- Page Container -->
<!--

<div id="page-container">

-->
<!-- Main Container -->
<main id="main-container">

    <!-- Page Content -->
    <div class="bg-image">
        <div class="hero-static bg-black-50">
            <div class="content">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-4">
                        <!-- Unlock Block -->
                        <div class="block block-themed mb-0">
                            <div class="block-header bg-danger">
                                <h3 class="block-title">Logout Missed</h3>
                                <div class="block-options">
                                    <!--nothing-->
                                </div>
                            </div>

                            <div class="block-content">
                                <p class="alert-danger text-center">Did You Forget To Submit Your Last Logout ??</p>
                                <div class="p-sm-3 px-lg-4  text-center">
                                    {{--                                    <img class="img-avatar img-avatar96" src="{{auth()->user()->profile_picture}}" alt="">--}}
                                    <p class="font-w600 my-2">
                                        {{auth()->user()->name}}
                                    </p>

                                    <!-- Unlock Form -->
                                    <form id="forgetLogoutForm" action="{{ route('saveUpdateLogout') }}" method="POST">
                                        @csrf
                                        <div class="form-group py-1">
                                            <label>Your Last Login</label>
                                            <input type="text" readonly
                                                   class="form-control form-control-lg form-control-alt text-center"
                                                   id="forgLogin" name="forgLogin" value="{{$lastLogin->login}}">
                                        </div>
                                        <div class="form-group py-1">
                                            <label>Please Enter Your Last Logout Time</label>
                                            <input type="time"
                                                   class="form-control form-control-lg form-control-alt text-center"
                                                   id="forgLogout" name="forgLogout" placeholder="set time">
                                            <span class="text-danger">{{ $errors->first('forgLogout') }}</span>
                                            <input type="time"
                                                   class="form-control form-control-lg form-control-alt text-center"
                                                   hidden value="{{$logintime}}" id="loginTime" name="loginTime"
                                                   placeholder="loginTime">

                                        </div>
                                        <div class="form-group row justify-content-center">
                                            <div class="col-md-6 col-xl-5">
                                                <button type="submit" class="btn btn-block btn-light">
                                                    <i class="fa fa-fw fa-check mr-1"></i> Submit
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Unlock Form -->
                                </div>
                            </div>
                        </div>
                        <!-- END Unlock Block -->
                    </div>
                </div>
            </div>
            <div class="content content-full font-size-sm text-white text-center">
                <strong>404 Developers</strong> <span data-toggle="year-copy">2019</span>
            </div>
        </div>
    </div>
    <!-- END Page Content -->

</main>
<!-- END Main Container -->
</div>
<!-- END Page Container -->

<!--
    OneUI JS Core

    Vital libraries and plugins used in all pages. You can choose to not include this file if you would like
    to handle those dependencies through webpack. Please check out assets/_es6/main/bootstrap.js for more info.

    If you like, you could also include them separately directly from the assets/js/core folder in the following
    order. That can come in handy if you would like to include a few of them (eg jQuery) from a CDN.

    assets/js/core/jquery.min.js
    assets/js/core/bootstrap.bundle.min.js
    assets/js/core/simplebar.min.js
    assets/js/core/jquery-scrollLock.min.js
    assets/js/core/jquery.appear.min.js
    assets/js/core/js.cookie.min.js
-->


<!--
    OneUI JS

    Custom functionality including Blocks/Layout API as well as other vital and optional helpers
    webpack is putting everything together at assets/_es6/main/app.js
-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--template-->

<script src="{{asset('js/core/jquery.min.js')}}"></script>

<script src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('js/core/simplebar.min.js')}}"></script>

<script src="{{asset('js/core/jquery-scrollLock.min.js')}}"></script>

<script src="{{asset('js/core/jquery.appear.min.js')}}"></script>

<script src="{{asset('js/core/js.cookie.min.js')}}"></script>

<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>


<script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('js/Employees/forgetLogoutValidation.js')}}"></script>

<script src="{{asset('js/pages/op_auth_lock.min.js')}}"></script>

<script src="{{asset('js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('js/plugins/jquery-validation/additional-methods.min.js')}}"></script>

</body>
</html>

