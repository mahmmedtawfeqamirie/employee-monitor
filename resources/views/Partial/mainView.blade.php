<!DOCTYPE html>
<html lang="en">
<head>
{{--    <title>404 Developer</title>--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="https://404developers.com/images/favicon.png" type="image/x-icon">

    <link rel="icon" href="https://404developers.com/images/favicon(3).png" type="image/x-icon">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

    <link rel="stylesheet" id="css-main" href="{{asset('css/oneui.min.css')}}">

    @yield('links')

</head>

<body>

<div id="page-container" class=" sidebar-dark enable-page-overlay side-scroll page-header-dark page-header-fixed sidebar-mini ">

    @include('Partial.navBar')

    <main id="main-container">

@yield('content')
        <input type="hidden" name="_token" id="main_token" value="{{csrf_token()}}">
    </main>

    @include('Partial.footer')

</div>

<!--scripts-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--template-->

<script src="{{asset('js/core/jquery.min.js')}}"></script>

<script src="{{asset('js/core/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('js/core/simplebar.min.js')}}"></script>

<script src="{{asset('js/core/jquery-scrollLock.min.js')}}"></script>

<script src="{{asset('js/core/jquery.appear.min.js')}}"></script>

<script src="{{asset('js/core/js.cookie.min.js')}}"></script>

<script src="{{asset('js/oneui.core.min.js')}}"></script>

<script src="{{asset('js/oneui.app.min.js')}}"></script>

<script src="{{asset('js/Users/UsersRouting.js')}}"></script>

@yield('scripts')

</body>

</html>
