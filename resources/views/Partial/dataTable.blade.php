<div class="block">
    <div class="block-content block-content-full">
        <table class="table table-bordered table-striped table-vcenter"
               id="DataTable{{$index}}" role="grid" aria-describedby="DataTables_Table_1_info">
            <thead class="thead-dark">
            <tr role="row">
                @foreach($headDataTable[$index] as $head)
                    <th>{{$head['name']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody class="DataTableTbody" id="dateTableBody{{$index}}">
            @foreach($data[$index] as $record)
                <tr role="row" class="odd">
                    <td class="font-w600 font-size-sm descriptionMission">{{$record->description}}</td>
                    @if( $index == 1 )
                        <td class="font-w600 font-size-sm statusMission">{{$record->status}}</td>
                    @endif
                    <td class="font-w600 font-size-sm">{{$record->created_at}}</td>
                    @if( $index == 1 && !auth()->user()->is_Admin)
                        <td class="font-w600 font-size-sm">
                            <button class="form-control btn btn-primary editStatus" style="width: auto"
                                    value="{{$record->id}}" data-toggle="modal" data-target="#modalAddingMission"
                                    onclick="changeStatus()">
                                <i class="fas fa-edit"></i>
                            </button>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
