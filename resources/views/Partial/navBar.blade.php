<!-- Header -->
<header id="page-header">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="d-flex align-items-center">
            @if(auth()->user()->is_Admin)
                @include('Admins.navButton')
                <button style="display: none" type="button" class="btn btn-sm btn-primary push"
                        data-toggle="modal" data-target="#modalAddingMission" id="addMissionBtn" onclick="ShowModel()">
                    Add Mission
                    <i class="fa fa-fw fa-plus mr-1"></i>
                </button>
            @else
                <button type="button" class="btn btn-sm btn-primary push"
                        data-toggle="modal" data-target="#modalAddingMission" id="addMissionBtn" onclick="ShowModel()">
                    Add Mission
                    <i class="fa fa-fw fa-plus mr-1"></i>
                </button>
            @endif
        </div>
        <!-- END Left Section -->
        <!-- Right Section -->
        <div class="d-flex align-items-center">
            <!-- User Dropdown -->
            <div class="dropdown d-inline-block ml-2">
                <button type="button" class="btn btn-sm btn-dual" id="page-header-user-dropdown"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded" src="{{asset('media/picture/profile/'.auth()->user()->profile_picture)}}"
                         alt="Header Avatar"
                         style="width: 18px;">
                    <span
                        class="d-none d-sm-inline-block ml-1">{{auth()->user()->name}}</span>
                    <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm"
                     aria-labelledby="page-header-user-dropdown">
                    <div class="p-3 text-center bg-primary">
                        <img class="img-avatar img-avatar48 img-avatar-thumb"
                             src="{{asset('media/picture/profile/'.auth()->user()->profile_picture)}}"
                             alt="">
                    </div>
                    <div class="p-2">
                        <h5 class="dropdown-header text-uppercase">User Options</h5>
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="{{url("/dashboard/user/".auth()->user()->id)}}">
                            <span>Edit Profile</span>
                            <i class="si si-settings"></i>
                        </a>
                        <div role="separator" class="dropdown-divider"></div>
                        <h5 class="dropdown-header text-uppercase">Actions</h5>
                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                           href="{{route('logout')}}">
                            <span>Log Out</span>
                            <i class="si si-logout ml-1"></i>
                        </a>
                    </div>
                </div>
            </div>
            <!-- END User Dropdown -->
        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->
</header>
<!-- END Header -->
