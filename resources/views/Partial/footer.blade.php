<footer id="page-footer" class="bg-body-light">
    <div class="content py-3">
        <div class="row font-size-sm">
{{--            <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">--}}
{{--                Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" href="https://404developers.com" target="_blank">404 Developers</a>--}}
{{--            </div>--}}
            <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
{{--                <a class="font-w600">404 Dev_User 2.0</a> --}}
                © <span data-toggle="year-copy" class="js-year-copy-enabled">2020</span>
            </div>
        </div>
    </div>
</footer>

