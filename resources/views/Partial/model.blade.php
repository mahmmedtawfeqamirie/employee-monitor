<div class="modal" id="modalAddingMission" tabindex="-1" role="dialog" aria-labelledby="modal-block-vcenter"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Add Mission</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-fw fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="block-content font-size-sm">
                    <div id="missionNameDiv">
                        <label for="inputMission">Mission</label>
                        <input type="text" class="form-control" placeholder="Enter Mission" name="description"
                               id="inputMission">
                        <br>
                    </div>
                    <label for="inputStatus">Status</label>
                    <select class="form-control" name="status" id="inputStatus">
                        <option value="pending" selected>Pending</option>
                        <option value="progress">Progress</option>
                        @if(!auth()->user()->is_Admin)
                            <option value="done">Done</option>
                        @endif
                    </select>
                    <br>
                </div>
                <div class="block-content block-content-full text-right border-top row">
                    <button type="button" onclick="clearModel()" class="btn btn-sm btn-danger" data-dismiss="modal">
                        Close <i class="si si-close"></i></button>
                    <button type="button" onclick="sweet2AddingMission()" class="btn btn-sm btn-primary"
                            id="submitMission"
                            data-dismiss="modal">Ok <i class="si si-check ml-2"></i></button>
                    <button type="button" class="btn btn-sm btn-primary"
                            data-dismiss="modal" id="EditMission">edit status <i class="si si-check ml-2"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
