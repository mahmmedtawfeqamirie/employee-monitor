<!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->

<!--
Separate your table content with multiple tbody sections. Add one row and add the class .js-table-sections-header to a
tbody section to make it clickable. It will then toggle the next tbody section which can have multiple rows. Eg:

<tbody class="js-table-sections-header">One row</tbody>
<tbody>Multiple rows</tbody>
<tbody class="js-table-sections-header">One row</tbody>
<tbody>Multiple rows</tbody>
<tbody class="js-table-sections-header">One row</tbody>
<tbody>Multiple rows</tbody>

You can also add the class .show in your tbody.js-table-sections-header to make the next tbody section visible by default

need <script>jQuery(function(){ One.helpers(['table-tools-sections']); });</script>
for make row clickable
-->


<!-- Start Table Sections -->

<table class="table table-vcenter  js-table-sections" id="tableSection">
    <thead class="table-dark">
    <tr>
        @foreach($headTableSection as $head)
            <th style="width: 900px;">{{$head['name']}}</th>
        @endforeach

    </tr>
    </thead>
    <tbody class="js-table-sections-header" id="tableSectionTbodyHead">
    <tr id="tableSectionTbodyTR">
        <td class="text-center">
            <i class="fa fa-angle-right text-muted"></i>
        </td>
        <td>
            <div class="py-1">
                <span>{{$first}}</span>
            </div>
        </td>
        <td>
            @if($last==null)
                <span class="badge badge-success">Active Now</span>
            @else
                <span>{{$last}}</span>
            @endif
        </td>
        <td class="d-none d-sm-table-cell">
            <span>{{$duration}}</span>
        </td>
    </tr>
    </tbody>
    <tbody class="font-size-sm" id="tableSectionTbodyData">
    <tr>
        <th style="width: 30px;"></th>
        <th>Login</th>
        <th style="width: 15%;">Logout</th>
    </tr>
    @if(!empty($schedule))
        @foreach($schedule as $sch)
            <tr>
                <td class="text-center"></td>
                <td class="font-w600 font-size-sm">{{$sch->login}}</td>
                @if($sch->logout==null)
                    <td class="font-w600 font-size-sm">Until Now</td>
                @else
                    <td class="font-w600 font-size-sm">{{$sch->logout}}</td>
                @endif


            </tr>
        @endforeach
    @endif
    </tbody>


</table>

<!-- END Table Sections -->

