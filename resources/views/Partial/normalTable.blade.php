<table class="table table-vcenter">
    <thead class="thead-dark">
    @foreach($headNormalTable as $head)
        <th>{{$head['name']}}</th>
    @endforeach
    </thead>
    <tbody id="NormalTable">
    <tr>
        @foreach($missions as $miss)

            <td class="font-w600 font-size-sm">
                <span >{{$miss->description}}</span>
            </td>
            <td class="font-w600 font-size-sm">
                <span >{{$miss->status}}</span>
            </td>
    </tr>
    @endforeach

    </tbody>
</table>


