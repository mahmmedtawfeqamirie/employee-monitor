
if ($("#AddNewUser").length > 0) {
    $("#AddNewUser").validate({

        rules: {
            NewUserName: {
                required: true,
                maxlength: 50,
                pattern:/^[A-Za-z\s]+$/,
            },

            NewUserSection: {
                required: true,
            },
            NewUserPhone: {
                digits:true,
            },
            RentredNewPassword: {
                equalTo:"#NewPassword"
            },
        },
        messages: {

            NewUserName: {
                required: "Please enter name",
                maxlength: "Your last name maxlength should be 50 characters long.",
                pattern:"no number allowd"
            },
            NewUserSection: {
                required: "Please enter Any Section",

            },
            NewUserSection: {
                digits: "Please enter only numbers",

            },
            RentredNewPassword: {
                equalTo:"Un matched Password"
            },

        },
    })
}

