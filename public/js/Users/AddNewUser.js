$('#NewUserPicture').change(function(){
    /*
    update The selected Photo on picture Section when Adding New User
     */
    let reader = new FileReader();
    reader.onload = (e) => {
        $('#imageContainer').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);

});
