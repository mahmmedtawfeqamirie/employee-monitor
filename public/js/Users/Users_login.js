

function pass_login()
{
    const Login_token=$("#Login_token");
    const loginPassword=$("#loginPassword").val();
    const loginId=$("#loginId").val();
    const token=Login_token.val();
    console.log(loginPassword);
    console.log(loginId);
    $.ajax({
        method: 'POST',
        url: 'passwordLogin',
        dataType: 'json',
        data: {
            password:loginPassword,
            id:loginId,
            _token: token
        }
    }).done((json) => {
        console.log(json);
        if(json.admin)
            window.location.href='dashboard';
        else  window.location.href='profile/'+json.userId;
    }).fail((json)=>{
        iziToast.error({
            title: 'Error',
            message: 'Wrong Id or Password',
            close:true,
            closeOnClick:true,
            position: 'topRight',
            timeout:3000,
            pauseOnHover:false,
        });
    });
}



$("#loginPassword").keyup(function(event) {
    console.log('pressed');
    if (event.keyCode === 13) {
        $("#loginBtn").click();
    }
});
