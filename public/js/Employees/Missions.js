function timeChange() {
    const date = $('#mission_Datepicker').val();
    const user = $('#user_id').val();
    updateMissionView(date, user, -1);
}

function updateMissionView(date, user, mission) {
    /*
    * mission =-1 Just Get Data
    * else Save Mission=mission
    *
    * */
    const token_html = $("#main_token");
    const tableSectionBodyData = $('#tableSectionTbodyData');
    const tableSectionTR = $('#tableSectionTbodyTR');
    const NormalTable = $('#NormalTable');
    console.log(user);
    $.ajax({
        method: 'GET',
        url: '/missUpdate',
        dataType: 'json',
        data: {
            date: date,
            id: user,
        },
    }).done((json) => {
        if (!json.type) {
            tableSectionTR.children().remove();
            tableSectionTR.append(fillTable(json, 1))
            tableSectionBodyData.children().remove();
            tableSectionBodyData.append(fillTable(json, 2));
            fillTableSectionChild(json.schedule, tableSectionBodyData);
            NormalTable.children().remove();
            fillNormalTable(json.missions, NormalTable);
        } else {
            tableSectionTR.children().remove();
            tableSectionBodyData.children().remove();
            NormalTable.children().remove();
        }
        console.log(json);
    }).fail((json) => {
        console.log(json);

    });
}

function fillNormalTable(mission, table) {
    var lenght = mission.length;
    for (var i = 0; i < lenght; i++) {
        table.append(fillTable(mission[i], 4))
    }

}

function fillTableSectionChild(schedule, table) {
    var lenght = schedule.length;
    for (var i = 0; i < lenght; i++) {
        table.append(fillTable(schedule[i], 3))
    }
}

function fillTable(json, type) {
    /*
    * Type1 Fill <tr> of Table Section Header
    *
    * typ2 Fill <tr> of Table Section Data <Headers>
    *
    * type3 Fill <tr> of Table Section Data <data>
    *
    * type4 Fill <tbody> of Normal Table
    * */

    if (type === 1) {
        var htmltext = `

                <td class="text-center">
                    <i class="fa fa-angle-right text-muted"></i>
                </td>
                <td>
                    <div class="py-1">
                        <span>${json.first}</span>
                    </div>
                </td>
                <td>`;
        if (json.last == null) {
            htmltext += `<span class="badge badge-success">Active Now</span>
                </td>`;
        } else
            htmltext += `<span>${json.last}</span>`;

        htmltext +=
            ` <td class="d-none d-sm-table-cell">
                    <span>${json.duration}</span>
                </td> `;
        return htmltext;
    } else if (type === 2) {
        return `<tr>
                <th style="width: 30px;"></th>
                <th>Login</th>
                <th style="width: 15%;">Logout</th>
            </tr>
           `
    } else if (type === 3) {
        var htmltext = `<tr>
                <td class="text-center"></td>
                <td >${json.login}</td>  `;
        if (json.logout == null)
            htmltext += `<td >Until Now</td>`;
        else
            htmltext += `<td >${json.logout}</td>`;
        htmltext += `</tr>`;
        return htmltext;
    } else if (type === 4) {
        return ` <tr>
                <td class="font-w600 font-size-sm">
                <span >${json.description}</span>
            </td>
            <td class="font-w600 font-size-sm">
                <span >${json.status}</span>
            </td>
            </tr>`
    }
}

function sweet2AddingMission() {
    const missionInput = $('#inputMission');
    const statusInput = $('#inputStatus');
    const model = $('#modalAddingMission');
    const user_id = $('#user_id').val();
    // console.log(user);
    if (missionInput.val() === "") {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Input your Mission!',
        }).then((result) => {
            model.modal('show');
        });

    } else {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Success!',
                    'Your Mission has been Submited.',
                    'success',
                    // updateMissionView(-1, user_id, missionInput.val(),statusInput.val()),
                    AddMission(user_id, missionInput.val(), statusInput.val()),
                    missionInput.val(null)
                )
            } else if (
                /* Cancel button Clicked */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                model.modal('show');
            }
        })


    }

}

function clearModel() {
    const missionInput = $('#inputMission');
    missionInput.val(null);
}

function AddMission(user_id, missionInput, statusInput) {

    const DataTable1 = $('#DataTable1');
    const tableSection = $('#NormalTable');

    const monthlyMissions_date = $('#monthMissionDatepicker').val();
    const activity_date = $('#mission_Datepicker').val();

    let missionsCount = document.getElementById("MissionsCount");

    $.ajax({
        url: '/addMission',
        method: 'get',
        dataType: 'json',
        data: {
            user_id: user_id,
            description: missionInput,
            status: statusInput,
            id: null
        }
    }).done((json) => {
        const mission = json[0].mission;
        const is_Admin = json[0].admin;
        if (monthlyMissions_date !== undefined)
            fillTableMissions(mission, DataTable1.DataTable(), monthlyMissions_date, is_Admin);
        else if (activity_date !== undefined)
            fillTableActivityMission(mission, tableSection, activity_date);
        else if (missionsCount !== null && mission.status === "done")
            missionsCount.textContent = (parseInt(missionsCount.textContent, 10) + 1).toString(10);
    }).fail(() => {
        console.log("Fail");
    });

}

function fillTableMissions(data, table, date, is_Admin) {
    let result = [];

    if (is_Admin)
        result.push([data.description, data.status, formatDate(data.created_at)]);
    else
        result.push([data.description, data.status, formatDate(data.created_at), GetEditButton(data)]);
    table.rows.add(result);

    if (date === GetTodayDate(false))
        table.draw();
}

function GetEditButton(json) {
    return ` <button class="form-control btn btn-primary editStatus" style="width: auto"
                                    value="${json.id}" data-toggle="modal" data-target="#modalAddingMission"
                                    onclick="changeStatus()">
                                <i class="fas fa-edit"></i>
                            </button>`;
}

function fillTableActivityMission(data, table, date) {
    if (date === GetTodayDate())
        table.append(getHTmlTable(data));
}

function getHTmlTable(json) {
    return `<tr>
                <td>${json.description}</td>
                <td>${json.status}</td>
           </tr>`
}

function GetTodayDate(day = true) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    if (day === true)
        today = dd + '/' + mm + '/' + yyyy;
    else
        today = mm + '/' + yyyy;
    return today;
}

function formatDate(date) {
    var m = new Date(date);
    var dateString =
        m.getUTCFullYear() + "-" +
        ("0" + (m.getUTCMonth() + 1)).slice(-2) + "-" +
        ("0" + m.getUTCDate()).slice(-2) + " " +
        ("0" + m.getUTCHours()).slice(-2) + ":" +
        ("0" + m.getUTCMinutes()).slice(-2) + ":" +
        ("0" + m.getUTCSeconds()).slice(-2);
    return dateString;
}
