$("#monthMissionDatepicker").datepicker({
    format: "mm/yyyy",
    viewMode: "months",
    minViewMode: "months",
    autoclose: true,
});
$('#DataTable1').DataTable({
    "deferRender": true,
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "info": true,
    "autoWidth": false,
    "aaSorting": [],
    "sDom": 'lfrtip',
    columnDefs: [
        {targets: 0, className: "descriptionMission"},
        {targets: 1, className: "statusMission"},
    ],
});
$('#DataTable2').DataTable({
    "deferRender": true,
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "info": true,
    "autoWidth": false,
    "aaSorting": [],
    "sDom": 'lfrtip'
});

function monthChange() {
    const date = $('#monthMissionDatepicker').val();
    const user = $('#user_id').val();
    console.log(date);
    updateMonthMissionView(date, user);
}

function updateMonthMissionView(date, user) {
    const tableMission = $('#DataTable1').DataTable();
    const tableNonAttend = $('#DataTable2').DataTable();
    $.ajax({
        url: '/updateMissionsPage',
        data: {
            date: date,
            user_id: user
        },
        method: 'GET',
        dataType: 'json'
    }).done((json) => {
        console.log(json.data[1]);
        fillDataTable(json.data[1], tableMission, 1, json.admin);
        fillDataTable(json.data[2], tableNonAttend, 2, json.admin);
    }).fail((json) => {
        console.log('fail');
    });
}

function fillDataTable(data, table, type, admin) {

    table.clear().draw();

    for (var i = 0; i < data.length; i++) {
        let result = [];
        if (type === 2)
            result.push([data[i].description, formatDate(data[i].created_at)]);
        else if (!admin)
            result.push([data[i].description, data[i].status, formatDate(data[i].created_at), new Mission().GetEditButton(data[i])]);// create A Object With Three Column (Like Data Table Row)
        else
            result.push([data[i].description, data[i].status, formatDate(data[i].created_at)]);// create A Object With Three Column (Like Data Table Row)

        table.rows.add(result); // add to DataTable instance
        table.draw(); // always redraw
    }
}

function formatDate(date) {
    var m = new Date(date);
    var dateString =
        m.getUTCFullYear() + "-" +
        ("0" + (m.getUTCMonth() + 1)).slice(-2) + "-" +
        ("0" + m.getUTCDate()).slice(-2) + " " +
        ("0" + m.getUTCHours()).slice(-2) + ":" +
        ("0" + m.getUTCMinutes()).slice(-2) + ":" +
        ("0" + m.getUTCSeconds()).slice(-2);
    return dateString;
}

function changeStatus() {
    document.getElementById('missionNameDiv').style.display = 'none';
    document.getElementById('submitMission').style.display = 'none';
    document.getElementById('EditMission').style.display = 'block';
}

class Mission {

    Initialize() {
        this.descriptionMission = $('.descriptionMission');
        this.statusMission = $('.statusMission');
        this.DataTable1 = $('#DataTable1').DataTable();
        return this;
    }

    BindEvents() {

        $(document).on('click', '.editStatus', (event) => {
            var current_row = $(event.target).closest('button');
            this.missionRow = $(event.target).closest('tr');
            this.mission_id = current_row.val();
            this.descriptionMission = current_row.closest('tr').find('.descriptionMission').text();
            this.user_id = $('#user_id').val();
            this.oldStatus = current_row.closest('tr').find('.statusMission');

            var select_status = document.getElementById('inputStatus');
            for (var i = 0; i < select_status.options.length; i++)
                if (select_status.options[i].text.toUpperCase() === this.oldStatus.text().toUpperCase())
                    select_status.options[i].selected = 'selected';

        });

        $(document).on('click', '#EditMission', () => {
            this.statusMission = $('#inputStatus').find(':selected').text().toLowerCase();
            console.log(this.statusMission);
            Swal.fire(
                'Success!',
                'Your Mission Status has been Changed',
                'success',
                this.UpdateMission(),
            );

        });

    }

    UpdateMission() {

        $.ajax({
            url: '/addMission',
            method: 'get',
            dataType: 'json',
            data: {
                user_id: this.user_id,
                description: this.descriptionMission,
                status: this.statusMission,
                id: this.mission_id
            }
        }).done((json) => {
            var mission = json[0].mission;
            this.DataTable1.row(this.missionRow).remove().draw();
            this.DataTable1.row.add
            ([
                mission.description,
                mission.status.toLowerCase(),
                formatDate(mission.created_at),
                this.GetEditButton(mission)
            ]);
            this.DataTable1.draw();
        }).fail((json) => {
            console.log("fail");
        })

    }

    GetEditButton(json) {
        return ` <button class="form-control btn btn-primary editStatus" style="width: auto"
                                    value="${json.id}" data-toggle="modal" data-target="#modalAddingMission"
                                    onclick="changeStatus()">
                                <i class="fas fa-edit"></i>
                            </button>`;
    }
}

function ShowModel() {
    document.getElementById('missionNameDiv').style.display = 'block';
    document.getElementById('submitMission').style.display = 'block';
    document.getElementById('EditMission').style.display = 'none';
}


$(document).ready(() => {
    new Mission().Initialize().BindEvents();
})
